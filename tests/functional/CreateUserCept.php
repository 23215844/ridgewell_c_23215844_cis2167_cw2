<?php
  $I = new FunctionalTester($scenario);

  $I->am('admin');
  $I->wantTo('create a new user');

  // When
  $I->amOnPage('/admin/users');
  $I->see('All Users', 'h1');
  $I->dontSee('Chloe Ridgewell');
  // And
  $I->click('Add User');

  // Then
  $I->amOnPage('/admin/users/create');
  // And
  $I->see('Add User', 'h1');
  $I->submitForm('.createuser', [
      'name' => 'Chloe Ridgewell',
      'email' => '23215844@edgehill.ac.uk',
      'password' => 'password'
  ]);
  // Then
  $I->seeCurrentUrlEquals('/admin/users');
  $I->see('Users', 'h1');
  $I->see('New user added!');
  $I->see('Chloe Ridgewell');



  // Check for duplicates

  // When
  $I->amOnPage('/admin/users');
  $I->see('Users', 'h1');
  $I->see('Chloe Ridgewell');
  // And
  $I->click('Add User');

  // Then
  $I->amOnPage('/admin/users/create');
  // And
  $I->see('Add User', 'h1');
  $I->submitForm('.createuser', [
      'name' => 'Chloe Ridgewell',
      'email' => '23215844@edgehill.ac.uk',
      'password' => 'password'
  ]);
  // Then
  $I->seeCurrentUrlEquals('/admin/users');
  $I->see('Users', 'h1');
  $I->see('Error user already exists!');
