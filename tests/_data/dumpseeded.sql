-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: ridgewell_c_23215844_cis2167_cw2
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.17.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `question_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer`
--

LOCK TABLES `answer` WRITE;
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2018_04_09_132932_create_roles_table',1),('2018_04_09_132944_create_permissions_table',1),('2018_04_09_132957_create_role_user_table',1),('2018_04_09_133008_create_permission_role_table',1),('2018_04_09_133018_create_questions_table',1),('2018_04_09_133028_create_answers_table',1),('2018_04_09_133035_create_results_table',1),('2018_04_09_133045_create_question_details_table',1),('2018_04_09_133052_create_questionnaires_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `questionnaires_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_detail`
--

DROP TABLE IF EXISTS `question_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_detail` (
  `question_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_detail`
--

LOCK TABLES `question_detail` WRITE;
/*!40000 ALTER TABLE `question_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `question_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaires`
--

DROP TABLE IF EXISTS `questionnaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaires` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `published_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `questionnaires_title_unique` (`title`),
  KEY `questionnaires_published_at_index` (`published_at`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaires`
--

LOCK TABLES `questionnaires` WRITE;
/*!40000 ALTER TABLE `questionnaires` DISABLE KEYS */;
INSERT INTO `questionnaires` VALUES (1,'Eos omnis maxime odit nisi.',1,'2018-04-15 13:10:05','2018-04-15 13:10:05','0000-00-00 00:00:00'),(2,'Dolores modi sit error iure sapiente nobis voluptas.',1,'2018-04-15 13:10:05','2018-04-15 13:10:05','0000-00-00 00:00:00'),(3,'Nihil vero totam debitis quos et.',1,'2018-04-15 13:10:05','2018-04-15 13:10:05','0000-00-00 00:00:00'),(4,'Laboriosam soluta dolor facere facilis eius aut ducimus.',1,'2018-04-15 13:10:05','2018-04-15 13:10:05','0000-00-00 00:00:00'),(5,'Sed consequatur exercitationem voluptates et molestiae id.',1,'2018-04-15 13:10:05','2018-04-15 13:10:05','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `questionnaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `result`
--

DROP TABLE IF EXISTS `result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `result` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `result` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `question_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `result`
--

LOCK TABLES `result` WRITE;
/*!40000 ALTER TABLE `result` DISABLE KEYS */;
/*!40000 ALTER TABLE `result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'chloe','chloe@example.com','$2y$10$jc/c4T/BFCUPolt1kOxyte.dDxou1sjNdA1SrX9C82WvjWhglrsIK','AkE3B01Itd',NULL,NULL),(2,'Mr. Pete Nienow','mortimer.grant@example.com','$2y$10$b5SeZU4t0mELRzUBcTJFF.IEbpUg67KkL5fxNuqrkzayzDKVQuWLe','jQ0zpAqXXB','2018-04-15 13:10:05','2018-04-15 13:10:05'),(3,'Miss Arielle Wisoky','emelia.toy@example.net','$2y$10$VSmQCi4sv.P9UPQmjGDtxee/QeCdYjVHQLqMZCRgLjEDFO32hHNQK','IkvlPQncKs','2018-04-15 13:10:05','2018-04-15 13:10:05'),(4,'Rita Weissnat','fpouros@example.net','$2y$10$06mCoIyH.pC2xQwWATVprOJI3P3VaM0RDQaxaC4puVtNMhFdfLnF6','IlEW4TpZd2','2018-04-15 13:10:05','2018-04-15 13:10:05'),(5,'Betsy Wunsch','kling.emiliano@example.org','$2y$10$wJ5gNWEHWVA36iIMRUG.cuZV5W2IUE5I8duy3YkDEeD/6aIoBmJly','3AaNAxH36p','2018-04-15 13:10:05','2018-04-15 13:10:05'),(6,'Ms. Ollie Bauch V','ksenger@example.net','$2y$10$G1dqw5yq3oC1oPkkzdKJqeShTOY53aTCsEbiY.dynw7bkQZsa0GKq','mfmuiswaKY','2018-04-15 13:10:05','2018-04-15 13:10:05'),(7,'Mr. Candelario Denesik','fhyatt@example.net','$2y$10$2zradqo2oVtOb/KcTtVageq3KqeuKZipg/Ni0.PdJxd.L.WMXaFY6','PEEpuGjVQl','2018-04-15 13:10:05','2018-04-15 13:10:05'),(8,'Nels Feest','yroberts@example.net','$2y$10$8okjD2OE6LxIW2M/uq5.SuAHX5XdatmNIMCYWRSJs7VVvOddHCewC','HeX8lT7MVb','2018-04-15 13:10:05','2018-04-15 13:10:05'),(9,'Mr. Ned Nikolaus','brenna.dooley@example.com','$2y$10$2zJMpnWTBoxfnW6M6aQaju0q9Z4diPofYA3bEkH5F5uSOMj.B40Oe','bJ8QaPN2JA','2018-04-15 13:10:05','2018-04-15 13:10:05'),(10,'Wellington Lesch','mwaelchi@example.com','$2y$10$QBYhxfjOmM.tAzMp3.rk0OBVQItywt2HgW9NziOkedJ6q2psFoPE.','0MYJIw1dcV','2018-04-15 13:10:05','2018-04-15 13:10:05'),(11,'Candace Reinger','cody.schmeler@example.net','$2y$10$MI3yQM0VUpitrOvOMptYH.80LzRw2ryHong/65cmoUAouD5mOQ9fy','EJO4A0hked','2018-04-15 13:10:05','2018-04-15 13:10:05'),(12,'Bradley Zulauf','qfranecki@example.net','$2y$10$wFXrAgp2Ur8rcYnw8sNNy.Dy22L6xAxOwIYTckusDlC2EwIpnALJ2','uzdevD4V0A','2018-04-15 13:10:05','2018-04-15 13:10:05'),(13,'Armando Hammes','fahey.nathaniel@example.org','$2y$10$vz0j7Bl0ws7akMhjZaZmJO91UhM3360lwzWHDVeqbeOYlPP0e8jsC','yYzFcypEVu','2018-04-15 13:10:05','2018-04-15 13:10:05'),(14,'Dr. Llewellyn Mann II','caleigh.strosin@example.com','$2y$10$K.UcF4p2FcVU7TdA2dx.Pej2HS6GXLkyoEkYpjocRc4dbOmCb8rde','vMCJRzjmES','2018-04-15 13:10:05','2018-04-15 13:10:05'),(15,'Alek Beatty','jhoeger@example.org','$2y$10$aaEzXnlYeXcEPAKO2ErFiec7sZEx4Zocva8KkVT2qN93e8PHWGryO','MTFqsH7sxB','2018-04-15 13:10:05','2018-04-15 13:10:05'),(16,'Prof. Cassidy Kulas II','gcormier@example.net','$2y$10$lMLnYA9di5bdrBJtKujMg.DFPnL662bpVNGeI5uwEaRQJBnV/EmOK','Fu5oMmjIDt','2018-04-15 13:10:05','2018-04-15 13:10:05'),(17,'Clyde Bailey','whitney83@example.net','$2y$10$uZZzCzufeuhhx8eTRRv1jur07Lij3lu.N8AhuRQqNmwARhmCayKaO','d8bPtHmX1c','2018-04-15 13:10:05','2018-04-15 13:10:05'),(18,'Mr. Ned Padberg II','nia.effertz@example.org','$2y$10$9/eoZhXxkBhTpj0CFwJ1peTF/FOlI4R4vu5tVJ/Yw/xd.FsSRViLC','pD4yl0Jxzz','2018-04-15 13:10:05','2018-04-15 13:10:05'),(19,'Norberto Leuschke','easter.kshlerin@example.com','$2y$10$Aeo1LuffhKjPjx8ruD7zse1AVwSTIzY.YdkVPuQN8tyHjdgQTaDCy','5T5VOwEd7D','2018-04-15 13:10:05','2018-04-15 13:10:05'),(20,'Mr. Hilton Halvorson','mraz.pauline@example.org','$2y$10$S9Bx1APPzV.Ypqy7B6/FL.r81gHd4bG6.mRbfwqfahkrgZvqZY47.','nAwNno3uLd','2018-04-15 13:10:05','2018-04-15 13:10:05'),(21,'Mr. Thurman Kassulke','theresa36@example.net','$2y$10$YcsuVsUEqoGIdJRtShflR.LibcD.C.fAu5eaBh2IDJYNkq6PVSJCW','0fHHaAKAY4','2018-04-15 13:10:05','2018-04-15 13:10:05'),(22,'Newton Douglas','trosenbaum@example.net','$2y$10$CbsKFbT1.MgVKzUQZU10D.bWt.X4khfvZDRmvfrmAjG3.JPQuTHvm','QoY39kZfoT','2018-04-15 13:10:05','2018-04-15 13:10:05'),(23,'Dr. Ryder Wisozk','taya33@example.org','$2y$10$mK1rEV8mw7.OAjl6SAZMZeQMthAXBoGoOGDrufjBb1Cpdn/GaLis.','IBYOTSwlBW','2018-04-15 13:10:05','2018-04-15 13:10:05'),(24,'Miss Jennie Ebert II','sienna.moen@example.net','$2y$10$4hzLOuZBq1pQ0.aieYAuuOdn1GruWQkAI8reQiH/WcA8bXTzchB.i','66K1j7lpTW','2018-04-15 13:10:05','2018-04-15 13:10:05'),(25,'Betsy Sipes','sidney.halvorson@example.org','$2y$10$3fR6tyTVD5hZsExkRXXx3.L9.cA7wCM3ty0b920lCIZCZFK6TLZpG','GtmWslcdTL','2018-04-15 13:10:05','2018-04-15 13:10:05'),(26,'Dr. Russell Hilpert','ida.pagac@example.com','$2y$10$tBbS5ed1mz1QU.I9G21Vq./hLMA2XyYah/fbI41qZusgGQUOoHH.6','10qj7DznXd','2018-04-15 13:10:05','2018-04-15 13:10:05'),(27,'Mr. Jaycee Gleason III','alexane47@example.com','$2y$10$Gr11l.NG.aEMRZgVpns6ruT52Lq3MMaFD6iNUI9VBA8LR.RSWlGY2','2QUF0yGwj8','2018-04-15 13:10:05','2018-04-15 13:10:05'),(28,'Macie Koepp','ashley.bradtke@example.com','$2y$10$khRobtOaqgUbTFy2ZDqid.3y9A2aeH6bRMgeELgrkvumeXOUIkTP.','ReXCvrUAuK','2018-04-15 13:10:05','2018-04-15 13:10:05'),(29,'Dr. Ruby Schiller DDS','cschultz@example.org','$2y$10$BuPdc3nrL0c1coCCSBhFXOSLiOjS89hAz3t2sSRu8SCo3UpGtSQXy','tiJG5jW9Su','2018-04-15 13:10:05','2018-04-15 13:10:05'),(30,'Summer Ratke','wisoky.elda@example.net','$2y$10$o6jPfmwYQ7WpTlKtC0nFKukix8nsL4/q7NscpkKpEYVtvLFHIE/na','4C4DdJ1mFl','2018-04-15 13:10:05','2018-04-15 13:10:05'),(31,'Devonte Olson','serena.white@example.org','$2y$10$243a.7SRK9ksmqu2aW5yw.BAzbWQ9LzyDjBxBP9em4Hs37xKtqEB6','igpjTUEEDb','2018-04-15 13:10:05','2018-04-15 13:10:05'),(32,'Yasmeen Homenick IV','oma79@example.com','$2y$10$aZJluoSRKNeMhL47UT.6Ee.LkBruPnSDmxVqAN6LkwOJKrrdXyz3u','83I2uvCPqU','2018-04-15 13:10:05','2018-04-15 13:10:05'),(33,'Dr. Kira Jast','virginie.abshire@example.net','$2y$10$YLEp2nbX4f2eYNJ6W54l8uO3U.lj9Z50Gyxnt/e/aCGM8buY3TNOC','HRBudTHt5M','2018-04-15 13:10:05','2018-04-15 13:10:05'),(34,'Mona Welch','marlen.batz@example.net','$2y$10$Hr/9XtBgLq5//wD70Xwx3.nFmTuaL3CgybP3qxIyAeLbkDDUkZaV2','SCS3fV64tp','2018-04-15 13:10:05','2018-04-15 13:10:05'),(35,'Dayne Nolan','schneider.arch@example.com','$2y$10$23qqAnhUQOB9CY1UYf8US.JRyfWOCQDuDJBhbhjpcPazOj6UvnNH2','mXum4yB1CY','2018-04-15 13:10:05','2018-04-15 13:10:05'),(36,'Burnice Schultz','jkonopelski@example.org','$2y$10$M3/QgHhV4oyBLdWC.dWye.SzqOd.h.9TkZDYYcHYud1j92uAeL4h2','pY0vDGlojp','2018-04-15 13:10:05','2018-04-15 13:10:05'),(37,'Ms. Dahlia Hartmann MD','harber.edwina@example.net','$2y$10$FKtGKfpzSGOFCCmmit3P9OJiYb3R8pmRSrCgKiPd/AYquwD7Afq9K','zHHEoKhLFX','2018-04-15 13:10:05','2018-04-15 13:10:05'),(38,'Filiberto Schowalter','hdaniel@example.org','$2y$10$.hL47Ud4jdgP4iyFf8Ssm.tqu2/FJ.2YJdCUl6AtFF/1JVa0djNOG','iwf0aEZR1n','2018-04-15 13:10:05','2018-04-15 13:10:05'),(39,'Prof. Cassandre Powlowski','feest.trent@example.org','$2y$10$X9c06Dd4IC581tyv4A/FHe2VecQo9CJWPbcEP9IeLJiyUA7UoL1cy','A1nJR0CET4','2018-04-15 13:10:05','2018-04-15 13:10:05'),(40,'Miss Nikita Hermiston II','bernhard.reagan@example.com','$2y$10$FFEwq.Ewjip0E/Qx7vY9ZO8L9JR2NkCrptaY7S3.ON8zV5gr9hFHa','yHBu7go0Xu','2018-04-15 13:10:05','2018-04-15 13:10:05'),(41,'Adela Adams','hammes.leif@example.net','$2y$10$944pINbZZEt0zScheMVS7ecrQOK5NljKvjnCtrWUTB3qHf5Igof7m','Y2JgLNY1am','2018-04-15 13:10:05','2018-04-15 13:10:05'),(42,'Dr. Lexie Farrell','vincenzo01@example.org','$2y$10$ccjoonerImnRYVNl.mRTZOxTwN8TgDR.ttuhKBkvCUEJnoD.Uzf6W','EjU3x0P4bW','2018-04-15 13:10:05','2018-04-15 13:10:05'),(43,'Arden Maggio','brook03@example.net','$2y$10$AH1FpMbibSkjq8u/MBr8yu9ivWpxR0x9r2sQ5ZUFN0w.NaDQiqSou','UWd4Fz4Fpl','2018-04-15 13:10:05','2018-04-15 13:10:05'),(44,'Shakira Kshlerin','nils.marvin@example.net','$2y$10$.xvnqc2PNRFnsXNoGq7uKu1xHqzdc/goUt2F6oYlhWihDzOzkN7Vi','2OqwSdRjKL','2018-04-15 13:10:05','2018-04-15 13:10:05'),(45,'Ciara Gulgowski','elena69@example.com','$2y$10$m1Kb/jyzqtFyO4m9rcevTeeBf00wbu2HZVZ1UO3kMwJ/sK32Gc9pG','7ZugFp700n','2018-04-15 13:10:05','2018-04-15 13:10:05'),(46,'Dr. Teresa Roberts','mcole@example.net','$2y$10$464d5gxhr1r3H1BLS1bip.qg9Hy0JIrxTG6PUE3b8zV2SoZrMc57a','LRb83h63s3','2018-04-15 13:10:05','2018-04-15 13:10:05'),(47,'Leopoldo Feest MD','patrick37@example.com','$2y$10$V7JsljSf8zlI/uCgqcS5kObCak/W46hJM64ipHapt7UVjNfnTf/Gy','axZDWTbs66','2018-04-15 13:10:05','2018-04-15 13:10:05'),(48,'Koby Hodkiewicz Sr.','vmuller@example.com','$2y$10$AZfN/l5xK8ro9t1CTeo3F.42TmP2xhosz8DQzvG0lnjgFDg0xN8tK','6Rgm4GrY0m','2018-04-15 13:10:05','2018-04-15 13:10:05'),(49,'Vince Little Jr.','ward.jorge@example.com','$2y$10$atypljhY1FIB.dXdRjfLmuENEz7UrG2fLQ3vEDRdaYdzNpSSNRLUq','QGe1TRyha7','2018-04-15 13:10:05','2018-04-15 13:10:05'),(50,'Shawn Hansen','smitham.edwina@example.org','$2y$10$37y30OWXuQwEl1CJBgyb3eeIJLs2iZ7Eko1p/dKGU41felXZ.8x8u','AgT1LOqOvG','2018-04-15 13:10:05','2018-04-15 13:10:05'),(51,'Prof. Danika Bailey I','crenner@example.org','$2y$10$bkcMmZmdUXJ9K/mrUf0PuekQMnVxdoKqD2xRKZELjIJkTOH2yKJw.','C8pQc4K6Oc','2018-04-15 13:10:05','2018-04-15 13:10:05');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-15 15:14:47
