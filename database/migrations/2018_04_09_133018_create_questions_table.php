<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('questions', function (Blueprint $table) {//the fields in the table with their data type
          $table->increments('id');
          $table->string('question');
          $table->integer('questionnaires_id');
          $table->string('answer1');
          $table->string('answer2');
          $table->string('answer3');
          $table->string('answer4');
          $table->string('answer5');
          $table->string('answer6');
          $table->timestamp('updated_at');
          $table->timestamp('created_at');
          $table->string('author_id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('questions');
    }
}
