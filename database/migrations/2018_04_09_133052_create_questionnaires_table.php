<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionnairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('questionnaires', function (Blueprint $table) {//the fields in the table with their data type
          $table->increments('id');
          $table->string('title')->unique(); //title which is string and must be unique
          $table->integer('author_id')->unsigned()->default(0);
          //$table->foreign('author_id')->references('id')->on('users')->onDelete('cascade');
          $table->timestamps();
          $table->timestamp('published_at')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('questionnaires');
    }
}
