<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
  public function permissions() { //the relationship with the permissions table
      return $this->belongsToMany(Permission::class);
  }

  public function givePermissionTo(Permission $permission) {
      return $this->permssion()->sync($permission);
  }
}
