<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
  protected $fillable = [
        'title',
    ];

  public function user()
  {
      return $this->belongsTo('App\User'); //the relationship with the user table
  }
}
