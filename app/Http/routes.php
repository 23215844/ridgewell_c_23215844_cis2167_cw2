<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/auth/logout', function()
{
    return view('auth/logout');
});

Route::get('/auth/login', function()
{
    return view('auth/login');
});



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () { /*routes group*/
  Route::auth();
  Route::get('/home', 'HomeController@index');
    Route::get('/', 'HomeController@index');
  Route::resource('/admin/questionnaires', 'QuestionnaireController' );
  Route::resource('/admin/questions', 'QuestionController' );
  Route::resource('/admin/results', 'ResultsController' );
  Route::resource('/admin/users', 'UserController' );
  Route::delete('/admin/users', 'UserController@destroy'); /*uses the destroy function in the controller*/
  Route::resource('/admin/takequestionnaire', 'TakeQuestionnaireController' );
});
