<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Question; //uses the question model
use Auth; //uses auth
use App\Answer; //uses the answer model
use App\Questionnaire; //uses the questionnaire model

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()//authenication is required
     {
         $this->middleware('auth');
     }

    public function index()
    {
      //get all questionnaires
      $questionnaire = Questionnaire::all();


      return view('admin/questionnaires', ['questionnaires' => $questionnaire]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/questionnaires/createquestion'); //redirects you to the createquestion page
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //$questionnaire = Questionnaire::create($request->all());
      $question = $request->all(); 
      $question['author_id'] = Auth::user()->id;
      $ques = Question::create($question);

      //$question->author_id = Auth::user()->id;
      //$input['author_id'] = Auth::user()->id;


      //return view('admin.questionnaires' ,['id' => $questionnaire->id] );

      //$answers = Answer::create($request->all());
      //TODO: Save question ID
      //$answers->save();

      return redirect('/admin/questionnaires');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $question = Question::findOrfail($id); //This code runs a find or fail on the id passed in

      $answers = Answer::where('question_id', $question->id)->get(); //gets answers relating to question id

      $question['answers'] = $answers;

      return view('admin.questions.show')->with('question', $question);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $question = Question::findOrFail($id); //This code runs a find or fail on the id passed in

      return view('admin/questions/edit', compact('question')); //if successful it will then return the admin questions edit view and pass the id in to the view

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) //used to update the questions
    {
      $question = question::findOrFail($id);//This code runs a find or fail on the id passed in

      $question->update($request->all()); //updates the database

      return redirect('admin/questionnaires'); //redirects to the questionnaire page
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) //used to delete the questions
    {
      $question = question::find($id);//This code runs a find or fail on the id passed in

      $question->delete();//this is deleted

      return redirect('admin/questions');//redirected back to the questions pahe
    }
}
