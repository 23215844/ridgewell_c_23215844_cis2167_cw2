<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Questionnaire;
use App\Question;//uses the question model
use App\Answer; //uses the answer model
use Auth;//uses the auth

class QuestionnaireController extends Controller
{
    /*
    * Secure the set of pages to the admin.
    */
    public function __construct()//authenication is required
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all the questionnaires
        $questionnaires = Questionnaire::all();

        return view('admin/questionnaires', ['questionnaires' => $questionnaires]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin/questionnaires/create'); //directed to the create questionnaire page
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //$questionnaire = Questionnaire::create($request->all());
        //$questionnaire->author_id = Auth::user()->id;
        //$questionnaire->save();
        $input = $request->all();
        $input['author_id'] = Auth::user()->id;
        $questionnaire = Questionnaire::create($input);

        return redirect('/admin/questionnaires/' . $questionnaire->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      // get the questionnaire
      $questionnaire = Questionnaire::findOrfail($id);

      $questions = Question::where('questionnaires_id', $questionnaire->id)->get();

      $questionnaire['questions'] = $questions;

      // if questionnaire does not exist return to list
      if(!$questionnaire)
      {
          return redirect('/admin/questionnaires');
        }
        return view('/admin/questionnaires/show')->withQuestionnaire($questionnaire);
      }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $questionnaire = Questionnaire::where('id',$id)->first(); //gets the questionnaire

        // if does not exist return to list
        if(!$questionnaire)
        {
          return redirect('/admin/questionnaires');
        }
        return view('admin/questionnaires/edit')->with('questionnaire', $questionnaire);//directed to edit page
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $questionnaire = Questionnaire::findOrFail($id);//This code runs a find or fail on the id passed in

      $questionnaire->update($request->all());//this is updated in the database

      return redirect('/admin/questionnaires');//redirected to the quesionnaire page
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $questionnaire = questionnaire::find($id);//This code runs a find or fail on the id passed in

      $questionnaire->delete();//this is deleted

      return redirect('admin/questionnaires'); //redirected back to the questionnaire page
    }
}
