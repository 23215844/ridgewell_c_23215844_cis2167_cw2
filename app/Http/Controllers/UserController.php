<?php

namespace App\Http\Controllers;
use Gate;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\User; //uses the user model
use App\Role; //uses the role model

class UserController extends Controller
{
    /*
    * Secure the set of pages to the admin.
    */
    public function __construct()//authenication is required
    {
      $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // if (Gate::allows('see_all_users')){

         $users = User::all();

         return view('admin/users/index', ['users' => $users]);
    //  }
    //  return view('/home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      // get the user
      $user = User::where('id',$id)->first();
      $roles = Role::all();

        // if user does not exist return to list
        if(!$user)
        {
          return redirect('/admin/users');

        }
        return view('admin/users/edit')->with('user', $user)->with('roles', $roles);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $user = User::findOrFail($id); //This code runs a find or fail on the id passed in
      $roles = $request->get('role');

      $user->roles()->sync($roles);
      $user->update($request->all()); //updates the user

      return redirect('/admin/users'); //directs back to the user page
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $user = user::find($id);//finds the user

      $user->delete();//deletes the user

      return redirect('/admin/users');//redirects to the user page
    }
}
