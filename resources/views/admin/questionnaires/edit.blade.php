@extends('layouts.app')<!--uses the file layouts.app for the navigation bar -->

@section('title', 'Edit {{ $questionnaire->title }}')

@section('content')
<!-- all content within a panel -->
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h1>Edit - {{ $questionnaire->title }}</h1></div><!--heading of the panel is the title of the questionnaire which is being updated-->

                <div class="panel-body"><!-- body of the panel -->
                  <div class="col-md-10">
                  {!! Form::model($questionnaire, ['method' => 'PATCH', 'url' => '/admin/questionnaires/' . $questionnaire->id]) !!}<!-- opens edit form-->

                  <div>
                      {!! Form::label('title', 'Title:') !!} <!--text  box which will have the current questionnaire title in it whch can be changed-->
                      {!! Form::text('title', null) !!}
                  </div>

                  <div>
                      {!! Form::submit('Update Title')  <!-- this button will update the title of the questionnaire-->
                  </div>


                  {!! Form::close() !!}
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
