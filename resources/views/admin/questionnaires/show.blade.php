@extends('layouts.app')<!--uses the file layouts.app for the navigation bar -->

@section('title', 'Questionnaire')

@section('content')
<!-- all content within a panel -->
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h1>{{ $questionnaire->title }}</h1></div><!--heading of the panel-->

                <div class="panel-body"><!-- body of the panel -->
                  <div class="col-md-10">
                  <ul>
                    @foreach($questionnaire['questions'] as $question)
                      <li><a href="/admin/questions/{{ $question->id }}">{{ $question->question }}</a></li> <!-- link to the question page-->
                      <td>  {!! Form::open(['method' => 'DELETE', 'route' => ['admin.questions.destroy', $question->id]]) !!} <!--deletes the question using the destroy in the routes file-->
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}</td>
                    @endforeach
                  </ul>

                  {!! Form::open(array('action' => 'QuestionController@store', 'id' => 'createquestion')) !!} <!-- opens form to input question and answers using question controller-->
                          {{ csrf_field() }}
                          {{ Form::hidden('questionnaires_id', $questionnaire->id) }} <!--id for the questionnaire this question is for-->
                  <div class="row large-12 columns">
                      {!! Form::label('question', 'Question:') !!} <!-- text box to input question-->
                      {!! Form::text('question', null, ['class' => 'large-8 columns']) !!}
                  </div>

                  <div class="row large-12 columns">
                      {!! Form::label('answer1', 'Answer 1:') !!}<!-- text box to input option-->
                      {!! Form::text('answer1', null, ['class' => 'large-8 columns']) !!}
                  </div>

                  <div class="row large-12 columns">
                      {!! Form::label('answer2', 'Answer 2:') !!}<!-- text box to input option-->
                      {!! Form::text('answer2', null, ['class' => 'large-8 columns']) !!}
                  </div>

                  <div class="row large-12 columns">
                      {!! Form::label('answer3', 'Answer 3:') !!}<!-- text box to input option-->
                      {!! Form::text('answer3', null, ['class' => 'large-8 columns']) !!}
                  </div>

                  <div class="row large-12 columns">
                      {!! Form::label('answer4', 'Answer 4:') !!}<!-- text box to input option-->
                      {!! Form::text('answer4', null, ['class' => 'large-8 columns']) !!}
                  </div>

                  <div class="row large-12 columns">
                      {!! Form::label('answer5', 'Answer 5:') !!}<!-- text box to input option-->
                      {!! Form::text('answer5', null, ['class' => 'large-8 columns']) !!}
                  </div>

                  <div class="row large-12 columns">
                      {!! Form::label('answer6', 'Answer 6:') !!}<!-- text box to input option-->
                      {!! Form::text('answer6', null, ['class' => 'large-8 columns']) !!}
                  </div>

                  <div class="row large-4 columns">
                        {!! Form::submit('Save Question', ['class' => 'button']) !!}<!--submits the question and answers-->
                  </div>
                    {!! Form::close() !!}
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
<section>
    <h1></h1>




</section>
@endsection
