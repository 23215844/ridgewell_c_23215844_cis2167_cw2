@extends('layouts.app') <!--uses the file layouts.app for the navigation bar -->

@section('title', 'Create Questionnaire')

@section('content')
<!-- all content within a panel -->
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h1>Create Questionnaire</h1></div> <!--heading of the panel-->

                <div class="panel-body"> <!-- body of the panel -->
                  <div class="col-md-10">
                  {!! Form::open(array('action' => 'QuestionnaireController@store', 'id' => 'createquestionnaire')) !!} <!-- opens the form using the store in the questionnaire controller -->
                          {{ csrf_field() }}
                      <div class="row large-12 columns">
                          {!! Form::label('title', 'Title:') !!}
                          {!! Form::text('title', null, ['class' => 'large-8 columns']) !!} <!-- creates field for the questionnaire title to be input -->
                      </div>
                      <div class="row large-4 columns">
                          {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!} <!-- submit button which will add the questionnaire-->
                      </div>
                  {!! Form::close() !!}
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
