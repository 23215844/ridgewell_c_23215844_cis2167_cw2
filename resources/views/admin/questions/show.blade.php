@extends('layouts.app')<!--uses the file layouts.app for the navigation bar -->

@section('title', 'Questionnaire')

@section('content')
<!-- all content within a panel -->
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h1>{{ $question->question }}</h1></div><!--heading of the panel-->

                <div class="panel-body"><!-- body of the panel -->
                  <div class="col-md-10">
                  <table>
                      <tr>
                          <th>Answer 1</th> <!-- table headings-->
                          <th>Answer 2</th>
                          <th>Answer 3</th>
                          <th>Answer 4</th>
                          <th>Answer 5</th>
                          <th>Answer 6</th>
                      </tr>
                          <tr>
                            <td>{{ $question->answer1}}</td><!-- prints answer 1 reating to this question-->
                            <td>{{ $question->answer2}}</td><!-- prints answer 2 reating to this question-->
                            <td>{{ $question->answer3}}</td><!-- prints answer 3 reating to this question-->
                            <td>{{ $question->answer4}}</td><!-- prints answer 4 reating to this question-->
                            <td>{{ $question->answer5}}</td><!-- prints answer 5 reating to this question-->
                            <td>{{ $question->answer6}}</td><!-- prints answer 6 reating to this question-->
                            <td> <a href="/admin/questions/{{ $question->id }}/edit" class="btn btn-warning">Update Question and Answers</a></td><!--link to the edit page to edit the question and answers-->

                          </tr>
                  </table>
                  <div>
                </div>
            </div>
        </div>
    </div>
</div>





@endsection
