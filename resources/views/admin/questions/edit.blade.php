@extends('layouts.app')<!--uses the file layouts.app for the navigation bar -->

@section('title', 'Edit {{ $question->question }}')

@section('content')
<!-- all content within a panel -->
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h1>Edit - {{ $question->question }}</h1></div><!--heading of the panel-->

                <div class="panel-body"><!-- body of the panel -->
                  <div class="col-md-10">
                  {!! Form::model($question, ['method' => 'PATCH', 'url' => '/admin/questions/' . $question->id]) !!}<!--opens edit form-->

                  <div>
                      {!! Form::label('Question', 'Question:') !!}<!--text  box which will have the current question in it whch can be changed-->
                      {!! Form::text('question', null) !!}
                  </div>

                  <div>
                      {!! Form::label('answer1', 'Answer 1:') !!}<!--text  box which will have the current option in it whch can be changed-->
                      {!! Form::text('answer1', null) !!}
                  </div>

                  <div>
                      {!! Form::label('answer2', 'Answer 2:') !!}<!--text  box which will have the current option in it whch can be changed-->
                      {!! Form::text('answer2', null) !!}
                  </div>

                  <div>
                      {!! Form::label('answer3', 'Answer 3:') !!}<!--text  box which will have the current option in it whch can be changed-->
                      {!! Form::text('answer3', null) !!}
                  </div>

                  <div>
                      {!! Form::label('answer4', 'Answer 4:') !!}<!--text  box which will have the current option in it whch can be changed-->
                      {!! Form::text('answer4', null) !!}
                  </div>

                  <div>
                      {!! Form::label('answer5', 'Answer 5:') !!}<!--text  box which will have the current option in it whch can be changed-->
                      {!! Form::text('answer5', null) !!}
                  </div>

                  <div>
                      {!! Form::label('answer6', 'Answer 6:') !!}<!--text  box which will have the current option in it whch can be changed-->
                      {!! Form::text('answer6', null) !!}
                  </div>

                  <div>
                      {!! Form::submit('Update Question and Answers') !!} <!--submits the question and answer replacing the previously saved question and answers-->
                  </div>
                    {!! Form::close() !!}


                  {!! Form::close() !!}
                </div>
              </div>
            </div>
        </div>
    </div>
</div>

@endsection
