@extends('layouts.app')<!--uses the file layouts.app for the navigation bar -->

@section('title', 'Results')

@section('content')
<!-- all content within a panel -->
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h1>Results</h1></div><!--heading of the panel-->

                <div class="panel-body"><!-- body of the panel -->
                  <div class="col-md-10">
                  <section>
                            @if (isset ($results))

                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <td>Question</td><!--table heading-->
                                            <td>Answer</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($results as $results)
                                            <tr>
                                                <td>{{ $results->question_id }}</td><!--prints question id-->
                                                <td>{{ $results->result }}</td><!-- prints result-->
                                            </tr>

                                        @endforeach
                                    </tbody>
                                </table>
                            @else
                                <p> No Results </p><!-- will print if no results-->
                            @endif
                        </section>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>




@endsection
