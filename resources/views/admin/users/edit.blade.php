@extends('layouts.app')<!--uses the file layouts.app for the navigation bar -->

@section('title', 'Edit {{ $user->name }}')

@section('content')
<!-- all content within a panel -->
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h1>Edit - {{ $user->name }}</h1></div><!--heading of the panel-->

                <div class="panel-body"><!-- body of the panel -->
                  <div class="col-md-10">
                  {!! Form::model($user, ['method' => 'PATCH', 'url' => '/admin/users/' . $user->id]) !!}<!-- opens edit form-->

                  <div>
                      {!! Form::label('name', 'Username:') !!}<!--text  box which will have the current username in it whch can be changed-->
                      {!! Form::text('name', null) !!}
                  </div>

                  <div>
                      {!! Form::label('email', 'Email Address:') !!}<!--text  box which will have the current email address in it whch can be changed-->
                      {!! Form::textarea('email', null) !!}
                  </div>

                  <div>
                      {!! Form::label('roles', 'Roles:') !!} <!-- checkbox to choose the users role-->
                      @foreach($roles as $role)
                          {{ Form::label($role->name) }}
                          {{ Form::checkbox('role[]', $role->id, $user->roles->contains($role->id), ['id' => $role->id]) }}
                      @endforeach

                  </div>

                  <div>
                      {!! Form::submit('Update User and Roles') !!}
                  </div>


                  {!! Form::close() !!}
                </div>
              </div>
            </div>
        </div>
    </div>
</div>

@endsection
