@extends('layouts.app')<!--uses the file layouts.app for the navigation bar -->

@section('title', 'All Users')

@section('content')
<!-- all content within a panel -->
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h1>All Users</h1></div><!--heading of the panel-->

                <div class="panel-body"><!-- body of the panel -->
                  <div class="col-md-10">
                  <section>
                      @if (isset ($users))

                          <table>
                              <tr>
                                  <th>Username</th> <!--table headings-->
                                  <th>email</th>
                                  <th>Permissions</th>
                              </tr>
                              @foreach ($users as $user)
                                  <tr>
                                      <td>{{ $user->name }}</td> <!-- prints the username-->
                                      <td> {{ $user->email }}</td><!--prints the email address-->
                                      <td>
                                          <ul>
                                              @foreach($user->roles as $role)
                                                  <li>{{ $role->label }}</li> <!--prints the role selected-->
                                              @endforeach
                                          </ul>
                                      </td>
                                      <td> <a href="users/{{ $user->id }}/edit" class="btn btn-warning">Update</a></td><!--link to the update user page-->
                                      <td>  {!! Form::open(['method' => 'DELETE', 'route' => ['admin.users.destroy', $user->id]]) !!}<!--button to delete the user-->
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                            {!! Form::close() !!}</td>
                                  </tr>
                              @endforeach
                          </table>
                      @else
                          <p>no users</p> <!--if there are no users this will be printed-->
                      @endif
                  </section>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>



@endsection
