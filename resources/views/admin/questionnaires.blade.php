@extends('layouts.app')<!--uses the file layouts.app for the navigation bar -->

@section('title', 'View Questionnaire')

@section('content')
<!-- all content within a panel -->
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h1>View Questionnaire</h1></div><!--heading of the panel-->

                <div class="panel-body"><!-- body of the panel -->
                  <div class="col-md-10">
                  <section>
                      @if (isset ($questionnaires))

                        <ul>
                            @foreach ($questionnaires as $questionnaire)
                              <li><a href="/admin/questionnaires/{{ $questionnaire->id }}" name="{{ $questionnaire->title }}">{{ $questionnaire->title }}</a></li><!--prints the questionnaire title and is a link to the questionnaire page-->
                              <td> <a href="questionnaires/{{ $questionnaire->id }}/edit" class="btn btn-warning">Update</a></td><!--link to the update questionnaire title page-->
                              <td>  {!! Form::open(['method' => 'DELETE', 'route' => ['admin.questionnaires.destroy', $questionnaire->id]]) !!}<!--deletes the questionnaire using destroy in the route-->
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}</td>
                            @endforeach
                          </ul>
                      @else
                          <p> no questionnaires added yet </p><!--if no questionnaires this is printed-->
                      @endif
                  </section>

                  {{ Form::open(array('action' => 'QuestionnaireController@create', 'method' => 'get')) }}<!--opens form using the create function in the questionnaire controller-->
                      <div class="row">
                          {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}<!--takes you to the create questionnaire page-->
                      </div>
                  {{ Form::close() }}
                </div>
              </div>
            </div>
        </div>
    </div>
</div>


@endsection
